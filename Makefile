
#**********************************************************
#
#   filename: Makefile
#
#   description: Makefile for HW5
#
#   author: Tyler Berkshire
#   login id: FA_19_CPS444_02
#
#   class:  CPS 444
#   instructor:  Perugini
#   assignment:  Homework 5
#
#   assigned: Sept 17, 2019
#   due: Sept 23, 109
#
#**********************************************************

PGM = keeplog
CC = gcc
LIB = -llist
KLIB1 = -lkeeplog_helper1
KLIB2 = -lkeeplog_helper2
OPTS = -c -DBSD -DNDEBUG
SRC1 = $(wildcard *.c) # list of all .c files
SRC = $(patsubst %.c,%,$(SRC1)) # list of all .c with .c removed
OBJECTS = $(addsuffix .o,$(SRC)) # list of all .o files

all: $(PGM)

$(PGM): $(OBJECTS)
	$(CC) -s -o $(PGM) $(OBJECTS) $(KLIB1) $(KLIB2) $(LIB)

$(OBJECTS): %.o: %.c
	$(CC) $(OPTS) $< -o $@
	$(CC) -MM $< > $*.d

-include $(addsuffix .d,$(SRC))

clean:
	@-rm *.o *.d $(PGM) 
